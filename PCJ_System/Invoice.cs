﻿using CrystalDecisions.CrystalReports.Engine;
using PCJ_System.PrintReport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PCJ_System
{
    public partial class Invoice : Form
    {
        ReportDocument Cry = new ReportDocument();
        int InvoiceNo = In_certi.InvoiceNo;
        string InvoiceType = In_certi.InvType;
        public Invoice()
        {
            InitializeComponent();
        }

        private void Invoice_Load(object sender, EventArgs e)
        {
            try
            {
                DSReport obj = new DSReport();
                string path = "";

                path = "G:\\Finalsystem\\PCJ_System\\PrintReport\\CRPrintReport.rpt";

                //path = "D:\\JewelPOS\\FinalSystem\\PCJ_System\\PrintReport\\CRPrintReport.rpt";
                //path = Application.StartupPath + @"\PrintReport\CRPrintReport.rpt";
                Cry.Load(path);

                if (DB_CONNECTION.Connection().State == ConnectionState.Closed)
                {
                    DB_CONNECTION.Connection().Open();
                }
                SqlDataAdapter Sda1 = new SqlDataAdapter("GetInvoice", DB_CONNECTION.Connection());
                SqlDataAdapter Sda2 = new SqlDataAdapter("GetProducts", DB_CONNECTION.Connection());
                //SqlDataAdapter Sda3 = new SqlDataAdapter("GetPaidInCash", DB_CONNECTION.Connection());
                //SqlDataAdapter Sda4 = new SqlDataAdapter("GetPaidInCard", DB_CONNECTION.Connection());
                Sda1.SelectCommand.CommandType = CommandType.StoredProcedure;
                Sda1.SelectCommand.Parameters.AddWithValue("InvoiceNo", InvoiceNo);
                Sda1.SelectCommand.Parameters.AddWithValue("InvType", InvoiceType);

                Sda2.SelectCommand.CommandType = CommandType.StoredProcedure;
                Sda2.SelectCommand.Parameters.AddWithValue("InvoiceNo", InvoiceNo);
                Sda2.SelectCommand.Parameters.AddWithValue("InvType", InvoiceType);

                //Sda3.SelectCommand.CommandType = CommandType.StoredProcedure;
                //Sda3.SelectCommand.Parameters.AddWithValue("InvNo", InvoiceNo);
                //Sda3.SelectCommand.Parameters.AddWithValue("InvType", InvoiceType);

                //Sda4.SelectCommand.CommandType = CommandType.StoredProcedure;
                //Sda4.SelectCommand.Parameters.AddWithValue("InvNo", InvoiceNo);
                //Sda4.SelectCommand.Parameters.AddWithValue("InvType", InvoiceType);

                DataTable DS1 = new DataTable();
                DataTable DS2 = new DataTable();
                //  DataTable DS3 = new DataTable();
                // DataTable DS4 = new DataTable();

                Sda1.Fill(DS1);
                Sda2.Fill(DS2);
                // Sda3.Fill(DS3);
                // Sda4.Fill(DS4);

                if (DB_CONNECTION.Connection().State == ConnectionState.Open)
                {
                    DB_CONNECTION.Connection().Close();
                }
                obj.Tables["GetInvoice"].Merge(DS1.Copy(), true, MissingSchemaAction.Ignore);
                obj.Tables["GetProducts"].Merge(DS2.Copy(), true, MissingSchemaAction.Ignore);
                //    obj.Tables["GetPaidInCash"].Merge(DS3.Copy(), true, MissingSchemaAction.Ignore);
                ///   obj.Tables["GetPaidInCard"].Merge(DS4.Copy(), true, MissingSchemaAction.Ignore);
                Cry.SetDataSource(obj);
                crystalReportViewer1.ReportSource = Cry;
                crystalReportViewer1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
