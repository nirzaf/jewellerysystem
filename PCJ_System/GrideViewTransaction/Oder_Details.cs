﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCJ_System.GrideViewTransaction
{
    class Oder_Details
    {
        public int InvoiceID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string Invoice_No { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public string StockId { get; set; }
        public int Quantity { get; set; }
        public float Amount { get; set; }
        public string PaidIn { get; set; }
        public float TotalAmount { get; set; }
    }
}
