﻿using CrystalDecisions.CrystalReports.Engine;
using PCJ_System.PrintReport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PCJ_System
{
    public partial class Print_Certificate : Form
    {
        ReportDocument Cry = new ReportDocument();
        int InvoiceNo = In_certi.InvoiceNo;
        string InvoiceType = In_certi.InvType;
        int LineNo = In_certi.LineNo;
        public Print_Certificate()
        {
            InitializeComponent();
        }

        private void Print_Certificate_Load(object sender, EventArgs e)
        {
            try
            {
                DSCertificate obj = new DSCertificate();
                string path = "";

                path = "G:\\Finalsystem\\PCJ_System\\PrintReport\\CertificateGen.rpt";

                //path = "D:\\JewelPOS\\FinalSystem\\PCJ_System\\PrintReport\\CertificateGen.rpt";
                //path = Application.StartupPath + @"\PrintReport\CRPrintReport.rpt";
                Cry.Load(path);

                if (DB_CONNECTION.Connection().State == ConnectionState.Closed)
                {
                    DB_CONNECTION.Connection().Open();
                }
                SqlDataAdapter Sda = new SqlDataAdapter("GetCertificate", DB_CONNECTION.Connection());
                Sda.SelectCommand.CommandType = CommandType.StoredProcedure;
                Sda.SelectCommand.Parameters.AddWithValue("InvoiceNo", InvoiceNo);
                Sda.SelectCommand.Parameters.AddWithValue("InvType", InvoiceType);
                Sda.SelectCommand.Parameters.AddWithValue("LineNo", LineNo);

                DataTable DS = new DataTable();

                Sda.Fill(DS);

                if (DB_CONNECTION.Connection().State == ConnectionState.Open)
                {
                    DB_CONNECTION.Connection().Close();
                }
                obj.Tables["GetCertificate"].Merge(DS.Copy(), true, MissingSchemaAction.Ignore);
                Cry.SetDataSource(obj);
                crystalReportViewer1.ReportSource = Cry;
                crystalReportViewer1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
