﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace PCJ_System
{
    public partial class Status_of_Stocks : UserControl
    {
        SqlConnection conn;
        DataTable dt = new DataTable();
        // SqlDataAdapter adapt;

        // private const string select_query = "Exec ProductStatus";
        private const string select_query = "SELECT * FROM Status_of_Stocks ORDER BY StockID DESC";
        
        private const string select_Gems = "SELECT * FROM Status_of_Stocks where StockType ='Gems' ORDER BY StockID DESC";
        private const string select_jewelry = "SELECT * FROM Status_of_Stocks where StockType ='Jewellery' ORDER BY StockID DESC";

        private const string select_stockout  = "SELECT * FROM Status_of_Stocks where Qty <= 0 ORDER BY StockID DESC";
        private const string select_stockavailable = "SELECT * FROM Status_of_Stocks where Qty > 0 ORDER BY StockID DESC";

        int tableSize = 0;
        

        private void SearchDataGridView() {
            int search = 0;
            int topcount = 0;

            string searchstock = searchBox1.Text.ToUpper();
            if (searchstock.Length == 6 || searchstock.Length == 0 || searchstock.Length == 5)
            {
                DisplayDataofStocks();

                if (searchstock != "")
                {

                    for (int i = 0; i < tableSize - (search + topcount); i++)
                    {
                        if (searchstock == Convert.ToString(dataGridView1.Rows[i].Cells[0].Value))
                        {
                            string stockNo = dataGridView1.Rows[i].Cells[0].Value.ToString();
                            string stockType = dataGridView1.Rows[i].Cells[1].Value.ToString();
                            string quantity = dataGridView1.Rows[i].Cells[2].Value.ToString();
                            string weight = dataGridView1.Rows[i].Cells[3].Value.ToString();
                            string cost = dataGridView1.Rows[i].Cells[4].Value.ToString();
                            string status = dataGridView1.Rows[i].Cells[5].Value.ToString();


                            dataGridView1.Rows.RemoveAt(i);

                            dataGridView1.Rows[search].Cells[0].Value = stockNo;
                            dataGridView1.Rows[search].Cells[1].Value = stockType;
                            dataGridView1.Rows[search].Cells[2].Value = quantity;
                            dataGridView1.Rows[search].Cells[3].Value = weight;
                            dataGridView1.Rows[search].Cells[4].Value = cost;
                            dataGridView1.Rows[search].Cells[5].Value = status;
                            search++;
                        }
                        else
                        {
                            topcount++;
                            dataGridView1.Rows.RemoveAt(i);
                            i--;

                        }
                    }
                }
                

                else
                {
                    DisplayDataofStocks();
                }
            }
        }

        private void AddDataToGridView(SqlDataReader reader)
        {
            dataGridView1.Rows.Clear();
            tableSize = 0;
            while (reader.Read())
            {
                //var row = dt.NewRow();
                tableSize++;
                var index = dataGridView1.Rows.Add();
                var row = dataGridView1.Rows[index];

                string sNo = reader["StockNo"].ToString();
                string sId = reader["StockID"].ToString();

                // store so that the items can be fetched later
                //StockNoList.Add(sNo);
                //StockIdList.Add(sId);
                string itemNo = String.Format("{0}{1}", sNo, sId);

                row.Cells[0].Value = itemNo;
                row.Cells[1].Value = reader["StockType"].ToString();
                row.Cells[2].Value = reader["Qty"].ToString();
                row.Cells[3].Value = reader["Weight"].ToString();
                row.Cells[4].Value = reader["Cost"].ToString();




                //  using (var command = new SqlCommand("SELECT TOP 1* FROM Status_of_Stocks WHERE StockNo=@StockNo AND StockID=@StockID AND StockType=@StockType", conn))
                // {


                //using (var reader1 = command.ExecuteReader())
                //{
                //    if (reader1.Read())
                //    {
                int qty = Int32.Parse(reader["Qty"].ToString());

                if (qty <= 0)
                {
                    row.Cells[5].Value = "Out of Stocks";
                }
                else
                {
                    row.Cells[5].Value = "Available";
                }
                //}
                //}
                // }
            }

            dataGridView1.RefreshEdit();
            conn.Close();
        }

        public void DisplayDataofStocks()
        {
            conn.Close();
            conn.Open();
            dataGridView1.Rows.Clear();
            // stockNoList.Clear();
            // stockItemList.Clear();
            // conn.Close();
            // conn.Open();

            try
            {
                var command = new SqlCommand(select_query, conn);
                using (var reader = command.ExecuteReader())
                {
                    AddDataToGridView(reader);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public Status_of_Stocks()
        {
            try
            {
                DB_CONNECTION dbObj = new DB_CONNECTION();
                conn = dbObj.getConnection();
            }

            catch (Exception ex)
            {
                MessageBox.Show("Can't Open Connection!! " + ex);
            }

            InitializeComponent();
            DisplayDataofStocks();
      
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
        }

        private void Status_of_Stocks_Load(object sender, EventArgs e)
        {
            DisplayDataofStocks();
        }

        private void btnrefresh_Click(object sender, EventArgs e)
        {
            DisplayDataofStocks();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

     


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                conn.Open();
                dataGridView1.Rows.Clear();
                var command = new SqlCommand(select_stockout, conn);
                var reader = command.ExecuteReader();
                AddDataToGridView(reader);
                // conn.Close(); 
         


            }
            else
            {
                conn.Open();
                dataGridView1.Rows.Clear();
                var command = new SqlCommand(select_stockavailable, conn);
                var reader = command.ExecuteReader();
                AddDataToGridView(reader);
                //   conn.Close();
            }
        }

      

        private void searchBox1_TextChanged(object sender, EventArgs e)
        {

            SearchDataGridView();
            /* DataView dv = dt.DefaultView;
             //Filter datagridview using textbox
             dv.RowFilter = string.Format(dataGridView1.Columns[0].HeaderText.ToString() + " like '%{0}%'", searchBox1.Text);
             dataGridView1.DataSource = dv.ToTable();*/
            //  SearchDataGridView();
        }

        private void searchBox1_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedIndex == 0)
            {
                conn.Open();
                dataGridView1.Rows.Clear();
                var command = new SqlCommand(select_Gems, conn);
                var reader = command.ExecuteReader();
                AddDataToGridView(reader);
                // conn.Close();
            }
            else
            {
                conn.Open();
                dataGridView1.Rows.Clear();
                var command = new SqlCommand(select_jewelry, conn);
                var reader = command.ExecuteReader();
                AddDataToGridView(reader);
                //   conn.Close();
            }
        }
    }

         
        
            
            
 }


