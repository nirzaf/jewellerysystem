﻿namespace PCJ_System
{
    partial class S_Jewelry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(S_Jewelry));
            this.txt_ID = new System.Windows.Forms.TextBox();
            this.Stock_Type = new System.Windows.Forms.TextBox();
            this.btnupdate = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnsave = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btn_delete = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label11 = new System.Windows.Forms.Label();
            this.hello = new System.Windows.Forms.Label();
            this.TB_File_Path = new System.Windows.Forms.TextBox();
            this.btn_Refresh = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_no_of_other_gems = new System.Windows.Forms.TextBox();
            this.txt_gem_weight = new System.Windows.Forms.TextBox();
            this.txt_cost = new System.Windows.Forms.TextBox();
            this.txt_weight_of_other_gems = new System.Windows.Forms.TextBox();
            this.txt_other_gems = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.txtstock_no = new System.Windows.Forms.TextBox();
            this.combo_itemk_description = new System.Windows.Forms.ComboBox();
            this.combo_item_type = new System.Windows.Forms.ComboBox();
            this.txt_no_of_gems = new System.Windows.Forms.TextBox();
            this.txt_qty = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_gem_type = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.currentimage1 = new System.Windows.Forms.Label();
            this.currentimage2 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_ID
            // 
            this.txt_ID.Location = new System.Drawing.Point(514, 77);
            this.txt_ID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_ID.Name = "txt_ID";
            this.txt_ID.Size = new System.Drawing.Size(177, 22);
            this.txt_ID.TabIndex = 0;
            this.txt_ID.Visible = false;
            // 
            // Stock_Type
            // 
            this.Stock_Type.Location = new System.Drawing.Point(514, 121);
            this.Stock_Type.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Stock_Type.Name = "Stock_Type";
            this.Stock_Type.Size = new System.Drawing.Size(177, 22);
            this.Stock_Type.TabIndex = 1;
            this.Stock_Type.Text = "Jewellery";
            this.Stock_Type.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Stock_Type.Visible = false;
            // 
            // btnupdate
            // 
            this.btnupdate.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnupdate.BackColor = System.Drawing.Color.DarkCyan;
            this.btnupdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnupdate.BorderRadius = 0;
            this.btnupdate.ButtonText = "  Update";
            this.btnupdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnupdate.DisabledColor = System.Drawing.Color.Gray;
            this.btnupdate.Iconcolor = System.Drawing.Color.Transparent;
            this.btnupdate.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnupdate.Iconimage")));
            this.btnupdate.Iconimage_right = null;
            this.btnupdate.Iconimage_right_Selected = null;
            this.btnupdate.Iconimage_Selected = null;
            this.btnupdate.IconMarginLeft = 0;
            this.btnupdate.IconMarginRight = 0;
            this.btnupdate.IconRightVisible = true;
            this.btnupdate.IconRightZoom = 0D;
            this.btnupdate.IconVisible = true;
            this.btnupdate.IconZoom = 60D;
            this.btnupdate.IsTab = false;
            this.btnupdate.Location = new System.Drawing.Point(1104, 77);
            this.btnupdate.Margin = new System.Windows.Forms.Padding(5);
            this.btnupdate.Name = "btnupdate";
            this.btnupdate.Normalcolor = System.Drawing.Color.DarkCyan;
            this.btnupdate.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnupdate.OnHoverTextColor = System.Drawing.Color.White;
            this.btnupdate.selected = false;
            this.btnupdate.Size = new System.Drawing.Size(204, 58);
            this.btnupdate.TabIndex = 11;
            this.btnupdate.Text = "  Update";
            this.btnupdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnupdate.Textcolor = System.Drawing.Color.White;
            this.btnupdate.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnupdate.Click += new System.EventHandler(this.btnupdate_Click);
            // 
            // btnsave
            // 
            this.btnsave.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnsave.BackColor = System.Drawing.Color.DarkCyan;
            this.btnsave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnsave.BorderRadius = 0;
            this.btnsave.ButtonText = "   SAVE";
            this.btnsave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnsave.DisabledColor = System.Drawing.Color.Gray;
            this.btnsave.ForeColor = System.Drawing.Color.Coral;
            this.btnsave.Iconcolor = System.Drawing.Color.Transparent;
            this.btnsave.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnsave.Iconimage")));
            this.btnsave.Iconimage_right = null;
            this.btnsave.Iconimage_right_Selected = null;
            this.btnsave.Iconimage_Selected = null;
            this.btnsave.IconMarginLeft = 0;
            this.btnsave.IconMarginRight = 0;
            this.btnsave.IconRightVisible = true;
            this.btnsave.IconRightZoom = 0D;
            this.btnsave.IconVisible = true;
            this.btnsave.IconZoom = 60D;
            this.btnsave.IsTab = false;
            this.btnsave.Location = new System.Drawing.Point(1104, 77);
            this.btnsave.Margin = new System.Windows.Forms.Padding(5);
            this.btnsave.Name = "btnsave";
            this.btnsave.Normalcolor = System.Drawing.Color.DarkCyan;
            this.btnsave.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnsave.OnHoverTextColor = System.Drawing.Color.White;
            this.btnsave.selected = false;
            this.btnsave.Size = new System.Drawing.Size(204, 58);
            this.btnsave.TabIndex = 10;
            this.btnsave.Text = "   SAVE";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnsave.Textcolor = System.Drawing.Color.White;
            this.btnsave.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btn_delete.BackColor = System.Drawing.Color.DarkCyan;
            this.btn_delete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_delete.BorderRadius = 0;
            this.btn_delete.ButtonText = "                 Delete";
            this.btn_delete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_delete.DisabledColor = System.Drawing.Color.Gray;
            this.btn_delete.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_delete.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_delete.Iconimage")));
            this.btn_delete.Iconimage_right = null;
            this.btn_delete.Iconimage_right_Selected = null;
            this.btn_delete.Iconimage_Selected = null;
            this.btn_delete.IconMarginLeft = 0;
            this.btn_delete.IconMarginRight = 0;
            this.btn_delete.IconRightVisible = true;
            this.btn_delete.IconRightZoom = 0D;
            this.btn_delete.IconVisible = true;
            this.btn_delete.IconZoom = 60D;
            this.btn_delete.IsTab = false;
            this.btn_delete.Location = new System.Drawing.Point(1318, 77);
            this.btn_delete.Margin = new System.Windows.Forms.Padding(5);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Normalcolor = System.Drawing.Color.DarkCyan;
            this.btn_delete.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btn_delete.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_delete.selected = false;
            this.btn_delete.Size = new System.Drawing.Size(204, 58);
            this.btn_delete.TabIndex = 12;
            this.btn_delete.Text = "                 Delete";
            this.btn_delete.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_delete.Textcolor = System.Drawing.Color.White;
            this.btn_delete.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_delete.Click += new System.EventHandler(this.bunifuFlatButton3_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(824, 89);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 17);
            this.label11.TabIndex = 88;
            this.label11.Text = "label11";
            this.label11.Visible = false;
            // 
            // hello
            // 
            this.hello.AutoSize = true;
            this.hello.Location = new System.Drawing.Point(717, 118);
            this.hello.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.hello.Name = "hello";
            this.hello.Size = new System.Drawing.Size(16, 17);
            this.hello.TabIndex = 93;
            this.hello.Text = "h";
            this.hello.Visible = false;
            // 
            // TB_File_Path
            // 
            this.TB_File_Path.ForeColor = System.Drawing.SystemColors.WindowText;
            this.TB_File_Path.Location = new System.Drawing.Point(110, 113);
            this.TB_File_Path.Margin = new System.Windows.Forms.Padding(4);
            this.TB_File_Path.Name = "TB_File_Path";
            this.TB_File_Path.Size = new System.Drawing.Size(178, 22);
            this.TB_File_Path.TabIndex = 113;
            this.TB_File_Path.Text = "C:\\\\STOCKIMAGES\\\\";
            this.TB_File_Path.Visible = false;
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btn_Refresh.BackColor = System.Drawing.Color.DarkCyan;
            this.btn_Refresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Refresh.BorderRadius = 0;
            this.btn_Refresh.ButtonText = "  Refresh";
            this.btn_Refresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Refresh.DisabledColor = System.Drawing.Color.Gray;
            this.btn_Refresh.Iconcolor = System.Drawing.Color.Transparent;
            this.btn_Refresh.Iconimage = ((System.Drawing.Image)(resources.GetObject("btn_Refresh.Iconimage")));
            this.btn_Refresh.Iconimage_right = null;
            this.btn_Refresh.Iconimage_right_Selected = null;
            this.btn_Refresh.Iconimage_Selected = null;
            this.btn_Refresh.IconMarginLeft = 0;
            this.btn_Refresh.IconMarginRight = 0;
            this.btn_Refresh.IconRightVisible = true;
            this.btn_Refresh.IconRightZoom = 0D;
            this.btn_Refresh.IconVisible = true;
            this.btn_Refresh.IconZoom = 60D;
            this.btn_Refresh.IsTab = false;
            this.btn_Refresh.Location = new System.Drawing.Point(1318, 77);
            this.btn_Refresh.Margin = new System.Windows.Forms.Padding(5);
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.Normalcolor = System.Drawing.Color.DarkCyan;
            this.btn_Refresh.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btn_Refresh.OnHoverTextColor = System.Drawing.Color.White;
            this.btn_Refresh.selected = false;
            this.btn_Refresh.Size = new System.Drawing.Size(204, 58);
            this.btn_Refresh.TabIndex = 114;
            this.btn_Refresh.Text = "  Refresh";
            this.btn_Refresh.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btn_Refresh.Textcolor = System.Drawing.Color.White;
            this.btn_Refresh.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Refresh.Click += new System.EventHandler(this.btn_Refresh_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(107)))), ((int)(((byte)(181)))));
            this.panel2.Controls.Add(this.pictureBox12);
            this.panel2.Controls.Add(this.pictureBox13);
            this.panel2.Controls.Add(this.pictureBox14);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1591, 52);
            this.panel2.TabIndex = 115;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(1823, 7);
            this.pictureBox12.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(44, 41);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 5;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.Click += new System.EventHandler(this.pictureBox12_Click);
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox13.Image")));
            this.pictureBox13.Location = new System.Drawing.Point(1863, 6);
            this.pictureBox13.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(48, 42);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 5;
            this.pictureBox13.TabStop = false;
            this.pictureBox13.Click += new System.EventHandler(this.pictureBox13_Click);
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(1776, 14);
            this.pictureBox14.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(39, 30);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 2;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.Click += new System.EventHandler(this.pictureBox14_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(13, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(442, 38);
            this.label1.TabIndex = 94;
            this.label1.Text = "Stock Entry for [JEWELRY]";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txt_no_of_other_gems
            // 
            this.txt_no_of_other_gems.Font = new System.Drawing.Font("Arial", 13F);
            this.txt_no_of_other_gems.Location = new System.Drawing.Point(895, 52);
            this.txt_no_of_other_gems.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_no_of_other_gems.Name = "txt_no_of_other_gems";
            this.txt_no_of_other_gems.Size = new System.Drawing.Size(225, 32);
            this.txt_no_of_other_gems.TabIndex = 6;
            this.txt_no_of_other_gems.TextChanged += new System.EventHandler(this.txt_no_of_other_gems_TextChanged);
            // 
            // txt_gem_weight
            // 
            this.txt_gem_weight.Font = new System.Drawing.Font("Arial", 13F);
            this.txt_gem_weight.Location = new System.Drawing.Point(317, 550);
            this.txt_gem_weight.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_gem_weight.Name = "txt_gem_weight";
            this.txt_gem_weight.Size = new System.Drawing.Size(272, 32);
            this.txt_gem_weight.TabIndex = 5;
            this.txt_gem_weight.TextChanged += new System.EventHandler(this.txt_gem_weight_TextChanged);
            // 
            // txt_cost
            // 
            this.txt_cost.Font = new System.Drawing.Font("Arial", 13F);
            this.txt_cost.Location = new System.Drawing.Point(1218, 100);
            this.txt_cost.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_cost.Name = "txt_cost";
            this.txt_cost.Size = new System.Drawing.Size(256, 32);
            this.txt_cost.TabIndex = 9;
            this.txt_cost.TextChanged += new System.EventHandler(this.txt_cost_TextChanged);
            // 
            // txt_weight_of_other_gems
            // 
            this.txt_weight_of_other_gems.Font = new System.Drawing.Font("Arial", 13F);
            this.txt_weight_of_other_gems.Location = new System.Drawing.Point(895, 130);
            this.txt_weight_of_other_gems.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_weight_of_other_gems.Name = "txt_weight_of_other_gems";
            this.txt_weight_of_other_gems.Size = new System.Drawing.Size(225, 32);
            this.txt_weight_of_other_gems.TabIndex = 8;
            this.txt_weight_of_other_gems.TextChanged += new System.EventHandler(this.txt_weight_of_other_gems_TextChanged);
            // 
            // txt_other_gems
            // 
            this.txt_other_gems.Font = new System.Drawing.Font("Arial", 13F);
            this.txt_other_gems.FormattingEnabled = true;
            this.txt_other_gems.Items.AddRange(new object[] {
            "DIAMONDS",
            "WHITE ZIRCOM",
            "WHITE SAPPHIRE",
            "BLUE SAPPHIRES",
            "RUBIES"});
            this.txt_other_gems.Location = new System.Drawing.Point(312, 629);
            this.txt_other_gems.Margin = new System.Windows.Forms.Padding(4);
            this.txt_other_gems.Name = "txt_other_gems";
            this.txt_other_gems.Size = new System.Drawing.Size(276, 33);
            this.txt_other_gems.TabIndex = 7;
            this.txt_other_gems.SelectedIndexChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txt_weight_of_other_gems);
            this.groupBox2.Controls.Add(this.txt_no_of_other_gems);
            this.groupBox2.Controls.Add(this.txt_cost);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(33, 503);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(1489, 215);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Measurement";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Arial", 14F);
            this.label15.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label15.Location = new System.Drawing.Point(596, 130);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(283, 27);
            this.label15.TabIndex = 39;
            this.label15.Text = "Weight of other Gems    :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Arial", 14F);
            this.label13.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label13.Location = new System.Drawing.Point(649, 55);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(230, 27);
            this.label13.TabIndex = 35;
            this.label13.Text = "No of other Gems   :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Arial", 14F);
            this.label16.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label16.Location = new System.Drawing.Point(1213, 71);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(96, 27);
            this.label16.TabIndex = 41;
            this.label16.Text = "Cost    :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Arial", 14F);
            this.label12.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label12.Location = new System.Drawing.Point(83, 41);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(182, 27);
            this.label12.TabIndex = 33;
            this.label12.Text = "Gem Weight    :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Arial", 14F);
            this.label14.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label14.Location = new System.Drawing.Point(87, 130);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(179, 27);
            this.label14.TabIndex = 37;
            this.label14.Text = "Other Gems    :";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // txtstock_no
            // 
            this.txtstock_no.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtstock_no.Enabled = false;
            this.txtstock_no.Font = new System.Drawing.Font("Arial", 13F);
            this.txtstock_no.Location = new System.Drawing.Point(173, 100);
            this.txtstock_no.Margin = new System.Windows.Forms.Padding(4);
            this.txtstock_no.Name = "txtstock_no";
            this.txtstock_no.Size = new System.Drawing.Size(161, 32);
            this.txtstock_no.TabIndex = 0;
            // 
            // combo_itemk_description
            // 
            this.combo_itemk_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.combo_itemk_description.Font = new System.Drawing.Font("Arial", 13F);
            this.combo_itemk_description.FormattingEnabled = true;
            this.combo_itemk_description.Items.AddRange(new object[] {
            "10KT YELLOW GOLD",
            "14KT YELLOW GOLD",
            "18KT YELLOW GOLD",
            "10KT WHITE GOLD",
            "9KT WHITE GOLD",
            "18KT WHITE GOLD",
            "PLATINUM",
            "SILVER",
            "14KT W/G + WHITE 14KT Y/G "});
            this.combo_itemk_description.Location = new System.Drawing.Point(896, 100);
            this.combo_itemk_description.Margin = new System.Windows.Forms.Padding(4);
            this.combo_itemk_description.Name = "combo_itemk_description";
            this.combo_itemk_description.Size = new System.Drawing.Size(310, 33);
            this.combo_itemk_description.TabIndex = 1;
            this.combo_itemk_description.SelectedIndexChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // combo_item_type
            // 
            this.combo_item_type.Font = new System.Drawing.Font("Arial", 13F);
            this.combo_item_type.FormattingEnabled = true;
            this.combo_item_type.Items.AddRange(new object[] {
            "RING",
            "PENDANT",
            "NECKLACE",
            "BRACELET",
            "CUFFLINK",
            "EARRING",
            "Broach",
            "Tie-Pin"});
            this.combo_item_type.Location = new System.Drawing.Point(173, 192);
            this.combo_item_type.Margin = new System.Windows.Forms.Padding(4);
            this.combo_item_type.Name = "combo_item_type";
            this.combo_item_type.Size = new System.Drawing.Size(198, 33);
            this.combo_item_type.TabIndex = 2;
            this.combo_item_type.SelectedIndexChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // txt_no_of_gems
            // 
            this.txt_no_of_gems.Font = new System.Drawing.Font("Arial", 13F);
            this.txt_no_of_gems.Location = new System.Drawing.Point(602, 196);
            this.txt_no_of_gems.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_no_of_gems.Name = "txt_no_of_gems";
            this.txt_no_of_gems.Size = new System.Drawing.Size(81, 32);
            this.txt_no_of_gems.TabIndex = 3;
            this.txt_no_of_gems.Text = "0";
            this.txt_no_of_gems.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_no_of_gems.TextChanged += new System.EventHandler(this.txt_no_of_gems_TextChanged);
            // 
            // txt_qty
            // 
            this.txt_qty.Font = new System.Drawing.Font("Arial", 13F);
            this.txt_qty.Location = new System.Drawing.Point(602, 101);
            this.txt_qty.Margin = new System.Windows.Forms.Padding(4);
            this.txt_qty.Name = "txt_qty";
            this.txt_qty.ReadOnly = true;
            this.txt_qty.Size = new System.Drawing.Size(81, 32);
            this.txt_qty.TabIndex = 3;
            this.txt_qty.Text = "01";
            this.txt_qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 14F);
            this.label8.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label8.Location = new System.Drawing.Point(434, 198);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(161, 27);
            this.label8.TabIndex = 67;
            this.label8.Text = "No of Gems  :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14F);
            this.label9.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label9.Location = new System.Drawing.Point(737, 201);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(151, 27);
            this.label9.TabIndex = 68;
            this.label9.Text = "Gem Type   :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 14F);
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(27, 193);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(138, 27);
            this.label7.TabIndex = 66;
            this.label7.Text = "Item Type  :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial", 14F);
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(745, 105);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 27);
            this.label4.TabIndex = 64;
            this.label4.Text = "Metal Type :";
            // 
            // txt_gem_type
            // 
            this.txt_gem_type.Font = new System.Drawing.Font("Arial", 13F);
            this.txt_gem_type.FormattingEnabled = true;
            this.txt_gem_type.Items.AddRange(new object[] {
            "BLUE SAPPHIRE",
            "RUBY",
            "STAR SAPPHIRE",
            "STAR RUBY",
            "CAT\'S EYE",
            "YELLOW SAPPHIRE"});
            this.txt_gem_type.Location = new System.Drawing.Point(896, 199);
            this.txt_gem_type.Margin = new System.Windows.Forms.Padding(4);
            this.txt_gem_type.Name = "txt_gem_type";
            this.txt_gem_type.Size = new System.Drawing.Size(310, 33);
            this.txt_gem_type.TabIndex = 4;
            this.txt_gem_type.SelectedIndexChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial", 14F);
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(24, 100);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 27);
            this.label3.TabIndex = 63;
            this.label3.Text = "Stock No   :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.currentimage1);
            this.groupBox1.Controls.Add(this.currentimage2);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txt_gem_type);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txt_qty);
            this.groupBox1.Controls.Add(this.txt_no_of_gems);
            this.groupBox1.Controls.Add(this.combo_item_type);
            this.groupBox1.Controls.Add(this.combo_itemk_description);
            this.groupBox1.Controls.Add(this.txtstock_no);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(32, 171);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(1490, 303);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Stock Details";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(36, 34);
            this.radioButton1.Margin = new System.Windows.Forms.Padding(4);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(162, 33);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.Text = "Main Stock";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(271, 34);
            this.radioButton2.Margin = new System.Windows.Forms.Padding(4);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(155, 33);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "Mini Stock";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 14F);
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label6.Location = new System.Drawing.Point(477, 101);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 27);
            this.label6.TabIndex = 65;
            this.label6.Text = "Quantity :";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(1339, 25);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(127, 117);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 116;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // currentimage1
            // 
            this.currentimage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage1.Location = new System.Drawing.Point(1335, 20);
            this.currentimage1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage1.Name = "currentimage1";
            this.currentimage1.Size = new System.Drawing.Size(140, 129);
            this.currentimage1.TabIndex = 126;
            // 
            // currentimage2
            // 
            this.currentimage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage2.Location = new System.Drawing.Point(1335, 163);
            this.currentimage2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage2.Name = "currentimage2";
            this.currentimage2.Size = new System.Drawing.Size(140, 129);
            this.currentimage2.TabIndex = 127;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Location = new System.Drawing.Point(1341, 168);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(127, 117);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 117;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox2.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // S_Jewelry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.ClientSize = new System.Drawing.Size(1591, 768);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btn_Refresh);
            this.Controls.Add(this.TB_File_Path);
            this.Controls.Add(this.txt_other_gems);
            this.Controls.Add(this.txt_gem_weight);
            this.Controls.Add(this.hello);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.Stock_Type);
            this.Controls.Add(this.txt_ID);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnupdate);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "S_Jewelry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "f";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.S_Jewelry_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.TextBox Stock_Type;
        public System.Windows.Forms.TextBox txt_ID;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label hello;
        public System.Windows.Forms.TextBox TB_File_Path;
        public Bunifu.Framework.UI.BunifuFlatButton btnupdate;
        public Bunifu.Framework.UI.BunifuFlatButton btn_delete;
        public Bunifu.Framework.UI.BunifuFlatButton btn_Refresh;
        public Bunifu.Framework.UI.BunifuFlatButton btnsave;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txt_no_of_other_gems;
        public System.Windows.Forms.TextBox txt_gem_weight;
        public System.Windows.Forms.TextBox txt_cost;
        public System.Windows.Forms.TextBox txt_weight_of_other_gems;
        public System.Windows.Forms.ComboBox txt_other_gems;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.ComboBox txt_gem_type;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txt_qty;
        public System.Windows.Forms.TextBox txt_no_of_gems;
        public System.Windows.Forms.ComboBox combo_item_type;
        public System.Windows.Forms.ComboBox combo_itemk_description;
        public System.Windows.Forms.TextBox txtstock_no;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        public System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label currentimage1;
        public System.Windows.Forms.RadioButton radioButton1;
        public System.Windows.Forms.RadioButton radioButton2;
        public System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label currentimage2;
    }
}