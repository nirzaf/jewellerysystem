﻿namespace PCJ_System
{
    partial class S_Gems
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(S_Gems));
            this.TB_File_Path = new System.Windows.Forms.TextBox();
            this.Stock_Type = new System.Windows.Forms.TextBox();
            this.txt_ID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.hello = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtstock_no = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtno_of_peices = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbStockType = new System.Windows.Forms.ComboBox();
            this.txt_cost = new System.Windows.Forms.TextBox();
            this.txt_weight = new System.Windows.Forms.TextBox();
            this.txt_gems = new System.Windows.Forms.ComboBox();
            this.btnsave = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnrefresh = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnupdate = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton3 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.currentimage10 = new System.Windows.Forms.Label();
            this.currentimage9 = new System.Windows.Forms.Label();
            this.currentimage8 = new System.Windows.Forms.Label();
            this.currentimage7 = new System.Windows.Forms.Label();
            this.currentimage6 = new System.Windows.Forms.Label();
            this.currentimage5 = new System.Windows.Forms.Label();
            this.currentimage4 = new System.Windows.Forms.Label();
            this.currentimage3 = new System.Windows.Forms.Label();
            this.currentimage2 = new System.Windows.Forms.Label();
            this.currentimage1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.currentimage11 = new System.Windows.Forms.Label();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.currentimage20 = new System.Windows.Forms.Label();
            this.currentimage19 = new System.Windows.Forms.Label();
            this.currentimage18 = new System.Windows.Forms.Label();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.currentimage30 = new System.Windows.Forms.Label();
            this.currentimage29 = new System.Windows.Forms.Label();
            this.currentimage28 = new System.Windows.Forms.Label();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.currentimage21 = new System.Windows.Forms.Label();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.currentimage12 = new System.Windows.Forms.Label();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.currentimage17 = new System.Windows.Forms.Label();
            this.currentimage16 = new System.Windows.Forms.Label();
            this.currentimage15 = new System.Windows.Forms.Label();
            this.currentimage14 = new System.Windows.Forms.Label();
            this.currentimage13 = new System.Windows.Forms.Label();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.currentimage22 = new System.Windows.Forms.Label();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.currentimage25 = new System.Windows.Forms.Label();
            this.currentimage24 = new System.Windows.Forms.Label();
            this.currentimage23 = new System.Windows.Forms.Label();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.currentimage26 = new System.Windows.Forms.Label();
            this.pictureBox33 = new System.Windows.Forms.PictureBox();
            this.currentimage27 = new System.Windows.Forms.Label();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            this.SuspendLayout();
            // 
            // TB_File_Path
            // 
            this.TB_File_Path.ForeColor = System.Drawing.SystemColors.WindowText;
            this.TB_File_Path.Location = new System.Drawing.Point(319, 82);
            this.TB_File_Path.Margin = new System.Windows.Forms.Padding(4);
            this.TB_File_Path.Name = "TB_File_Path";
            this.TB_File_Path.Size = new System.Drawing.Size(296, 22);
            this.TB_File_Path.TabIndex = 100;
            this.TB_File_Path.Text = "C:\\\\STOCKIMAGES\\\\";
            this.TB_File_Path.Visible = false;
            // 
            // Stock_Type
            // 
            this.Stock_Type.Location = new System.Drawing.Point(135, 126);
            this.Stock_Type.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Stock_Type.Name = "Stock_Type";
            this.Stock_Type.Size = new System.Drawing.Size(177, 22);
            this.Stock_Type.TabIndex = 1;
            this.Stock_Type.Text = "Gems";
            this.Stock_Type.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Stock_Type.Visible = false;
            // 
            // txt_ID
            // 
            this.txt_ID.Location = new System.Drawing.Point(135, 82);
            this.txt_ID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_ID.Name = "txt_ID";
            this.txt_ID.Size = new System.Drawing.Size(177, 22);
            this.txt_ID.TabIndex = 0;
            this.txt_ID.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(5, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(373, 38);
            this.label1.TabIndex = 21;
            this.label1.Text = "Stock Entry for [GEMS]";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(52, 82);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 17);
            this.label11.TabIndex = 22;
            this.label11.Text = "label11";
            this.label11.Visible = false;
            // 
            // hello
            // 
            this.hello.AutoSize = true;
            this.hello.Location = new System.Drawing.Point(52, 105);
            this.hello.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.hello.Name = "hello";
            this.hello.Size = new System.Drawing.Size(16, 17);
            this.hello.TabIndex = 23;
            this.hello.Text = "h";
            this.hello.Visible = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(107)))), ((int)(((byte)(181)))));
            this.panel2.Controls.Add(this.pictureBox12);
            this.panel2.Controls.Add(this.pictureBox13);
            this.panel2.Controls.Add(this.pictureBox14);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1575, 52);
            this.panel2.TabIndex = 102;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(1460, 6);
            this.pictureBox12.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(44, 41);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 5;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.Click += new System.EventHandler(this.pictureBox12_Click);
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox13.Image")));
            this.pictureBox13.Location = new System.Drawing.Point(1500, 5);
            this.pictureBox13.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(48, 42);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 5;
            this.pictureBox13.TabStop = false;
            this.pictureBox13.Click += new System.EventHandler(this.pictureBox13_Click);
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(1996, 10);
            this.pictureBox14.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(39, 30);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 2;
            this.pictureBox14.TabStop = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Gainsboro;
            this.button2.Location = new System.Drawing.Point(2045, 0);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(41, 43);
            this.button2.TabIndex = 3;
            this.button2.Text = "_";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Gainsboro;
            this.button1.Location = new System.Drawing.Point(2067, 5);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 43);
            this.button1.TabIndex = 2;
            this.button1.Text = "X";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // txtstock_no
            // 
            this.txtstock_no.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.txtstock_no.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtstock_no.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtstock_no.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.txtstock_no.ForeColor = System.Drawing.Color.White;
            this.txtstock_no.Location = new System.Drawing.Point(459, 45);
            this.txtstock_no.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtstock_no.Name = "txtstock_no";
            this.txtstock_no.ReadOnly = true;
            this.txtstock_no.Size = new System.Drawing.Size(117, 27);
            this.txtstock_no.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtstock_no);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtno_of_peices);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Location = new System.Drawing.Point(40, 164);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(1507, 230);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Stock Details";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial", 14F);
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(63, 49);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(228, 27);
            this.label4.TabIndex = 0;
            this.label4.Text = "Stock No                :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial", 14F);
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label6.Location = new System.Drawing.Point(692, 113);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(165, 27);
            this.label6.TabIndex = 3;
            this.label6.Text = "Weight          :";
            // 
            // txtno_of_peices
            // 
            this.txtno_of_peices.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic);
            this.txtno_of_peices.Location = new System.Drawing.Point(336, 113);
            this.txtno_of_peices.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtno_of_peices.Name = "txtno_of_peices";
            this.txtno_of_peices.Size = new System.Drawing.Size(269, 29);
            this.txtno_of_peices.TabIndex = 3;
            this.txtno_of_peices.Text = "0";
            this.txtno_of_peices.TextChanged += new System.EventHandler(this.txtno_of_peices_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial", 14F);
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(693, 176);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 27);
            this.label3.TabIndex = 4;
            this.label3.Text = "Price             :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 14F);
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(61, 176);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(223, 27);
            this.label2.TabIndex = 1;
            this.label2.Text = "Description            :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial", 14F);
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label5.Location = new System.Drawing.Point(63, 114);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(222, 27);
            this.label5.TabIndex = 2;
            this.label5.Text = "Nr. of Pcs              :";
            // 
            // cmbStockType
            // 
            this.cmbStockType.AllowDrop = true;
            this.cmbStockType.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbStockType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStockType.FormattingEnabled = true;
            this.cmbStockType.Items.AddRange(new object[] {
            "UG",
            "MG"});
            this.cmbStockType.Location = new System.Drawing.Point(379, 214);
            this.cmbStockType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbStockType.Name = "cmbStockType";
            this.cmbStockType.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cmbStockType.Size = new System.Drawing.Size(92, 24);
            this.cmbStockType.TabIndex = 2;
            this.cmbStockType.SelectedIndexChanged += new System.EventHandler(this.cmbStockType_SelectedIndexChanged);
            // 
            // txt_cost
            // 
            this.txt_cost.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic);
            this.txt_cost.Location = new System.Drawing.Point(939, 339);
            this.txt_cost.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_cost.Name = "txt_cost";
            this.txt_cost.Size = new System.Drawing.Size(251, 29);
            this.txt_cost.TabIndex = 6;
            this.txt_cost.Text = "0";
            this.txt_cost.TextChanged += new System.EventHandler(this.txtno_of_peices_TextChanged);
            // 
            // txt_weight
            // 
            this.txt_weight.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Italic);
            this.txt_weight.Location = new System.Drawing.Point(939, 275);
            this.txt_weight.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_weight.Name = "txt_weight";
            this.txt_weight.Size = new System.Drawing.Size(251, 29);
            this.txt_weight.TabIndex = 5;
            this.txt_weight.Text = "0";
            this.txt_weight.TextChanged += new System.EventHandler(this.txtno_of_peices_TextChanged);
            // 
            // txt_gems
            // 
            this.txt_gems.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_gems.FormattingEnabled = true;
            this.txt_gems.Items.AddRange(new object[] {
            "BLUE SAPPHIRE",
            "RUBY",
            "STAR SAPPHIRE",
            "STAR RUBY",
            "CAT\'S EYE",
            "YELLOW SAPPHIRE"});
            this.txt_gems.Location = new System.Drawing.Point(376, 339);
            this.txt_gems.Margin = new System.Windows.Forms.Padding(4);
            this.txt_gems.Name = "txt_gems";
            this.txt_gems.Size = new System.Drawing.Size(269, 25);
            this.txt_gems.TabIndex = 4;
            // 
            // btnsave
            // 
            this.btnsave.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnsave.BackColor = System.Drawing.Color.DarkCyan;
            this.btnsave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnsave.BorderRadius = 0;
            this.btnsave.ButtonText = "Save";
            this.btnsave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnsave.DisabledColor = System.Drawing.Color.Gray;
            this.btnsave.ForeColor = System.Drawing.Color.Coral;
            this.btnsave.Iconcolor = System.Drawing.Color.Transparent;
            this.btnsave.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnsave.Iconimage")));
            this.btnsave.Iconimage_right = null;
            this.btnsave.Iconimage_right_Selected = null;
            this.btnsave.Iconimage_Selected = null;
            this.btnsave.IconMarginLeft = 0;
            this.btnsave.IconMarginRight = 0;
            this.btnsave.IconRightVisible = true;
            this.btnsave.IconRightZoom = 0D;
            this.btnsave.IconVisible = true;
            this.btnsave.IconZoom = 50D;
            this.btnsave.IsTab = false;
            this.btnsave.Location = new System.Drawing.Point(1130, 83);
            this.btnsave.Margin = new System.Windows.Forms.Padding(5);
            this.btnsave.Name = "btnsave";
            this.btnsave.Normalcolor = System.Drawing.Color.DarkCyan;
            this.btnsave.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnsave.OnHoverTextColor = System.Drawing.Color.White;
            this.btnsave.selected = false;
            this.btnsave.Size = new System.Drawing.Size(204, 58);
            this.btnsave.TabIndex = 8;
            this.btnsave.Text = "Save";
            this.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnsave.Textcolor = System.Drawing.Color.White;
            this.btnsave.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // btnrefresh
            // 
            this.btnrefresh.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnrefresh.BackColor = System.Drawing.Color.DarkCyan;
            this.btnrefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnrefresh.BorderRadius = 0;
            this.btnrefresh.ButtonText = "                 Refresh";
            this.btnrefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnrefresh.DisabledColor = System.Drawing.Color.Gray;
            this.btnrefresh.Iconcolor = System.Drawing.Color.Transparent;
            this.btnrefresh.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnrefresh.Iconimage")));
            this.btnrefresh.Iconimage_right = null;
            this.btnrefresh.Iconimage_right_Selected = null;
            this.btnrefresh.Iconimage_Selected = null;
            this.btnrefresh.IconMarginLeft = 0;
            this.btnrefresh.IconMarginRight = 0;
            this.btnrefresh.IconRightVisible = true;
            this.btnrefresh.IconRightZoom = 0D;
            this.btnrefresh.IconVisible = true;
            this.btnrefresh.IconZoom = 60D;
            this.btnrefresh.IsTab = false;
            this.btnrefresh.Location = new System.Drawing.Point(1344, 84);
            this.btnrefresh.Margin = new System.Windows.Forms.Padding(5);
            this.btnrefresh.Name = "btnrefresh";
            this.btnrefresh.Normalcolor = System.Drawing.Color.DarkCyan;
            this.btnrefresh.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnrefresh.OnHoverTextColor = System.Drawing.Color.White;
            this.btnrefresh.selected = false;
            this.btnrefresh.Size = new System.Drawing.Size(204, 58);
            this.btnrefresh.TabIndex = 101;
            this.btnrefresh.Text = "                 Refresh";
            this.btnrefresh.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnrefresh.Textcolor = System.Drawing.Color.White;
            this.btnrefresh.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnrefresh.Click += new System.EventHandler(this.btnrefresh_Click);
            // 
            // btnupdate
            // 
            this.btnupdate.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnupdate.BackColor = System.Drawing.Color.DarkCyan;
            this.btnupdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnupdate.BorderRadius = 0;
            this.btnupdate.ButtonText = "     Update";
            this.btnupdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnupdate.DisabledColor = System.Drawing.Color.Gray;
            this.btnupdate.Iconcolor = System.Drawing.Color.Transparent;
            this.btnupdate.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnupdate.Iconimage")));
            this.btnupdate.Iconimage_right = null;
            this.btnupdate.Iconimage_right_Selected = null;
            this.btnupdate.Iconimage_Selected = null;
            this.btnupdate.IconMarginLeft = 0;
            this.btnupdate.IconMarginRight = 0;
            this.btnupdate.IconRightVisible = true;
            this.btnupdate.IconRightZoom = 0D;
            this.btnupdate.IconVisible = true;
            this.btnupdate.IconZoom = 60D;
            this.btnupdate.IsTab = false;
            this.btnupdate.Location = new System.Drawing.Point(1129, 84);
            this.btnupdate.Margin = new System.Windows.Forms.Padding(5);
            this.btnupdate.Name = "btnupdate";
            this.btnupdate.Normalcolor = System.Drawing.Color.DarkCyan;
            this.btnupdate.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnupdate.OnHoverTextColor = System.Drawing.Color.White;
            this.btnupdate.selected = false;
            this.btnupdate.Size = new System.Drawing.Size(204, 58);
            this.btnupdate.TabIndex = 9;
            this.btnupdate.Text = "     Update";
            this.btnupdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnupdate.Textcolor = System.Drawing.Color.White;
            this.btnupdate.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnupdate.Click += new System.EventHandler(this.btnupdate_Click);
            // 
            // bunifuFlatButton3
            // 
            this.bunifuFlatButton3.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton3.BackColor = System.Drawing.Color.DarkCyan;
            this.bunifuFlatButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton3.BorderRadius = 0;
            this.bunifuFlatButton3.ButtonText = "                Delete";
            this.bunifuFlatButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton3.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton3.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton3.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton3.Iconimage")));
            this.bunifuFlatButton3.Iconimage_right = null;
            this.bunifuFlatButton3.Iconimage_right_Selected = null;
            this.bunifuFlatButton3.Iconimage_Selected = null;
            this.bunifuFlatButton3.IconMarginLeft = 0;
            this.bunifuFlatButton3.IconMarginRight = 0;
            this.bunifuFlatButton3.IconRightVisible = true;
            this.bunifuFlatButton3.IconRightZoom = 0D;
            this.bunifuFlatButton3.IconVisible = true;
            this.bunifuFlatButton3.IconZoom = 50D;
            this.bunifuFlatButton3.IsTab = false;
            this.bunifuFlatButton3.Location = new System.Drawing.Point(1344, 84);
            this.bunifuFlatButton3.Margin = new System.Windows.Forms.Padding(5);
            this.bunifuFlatButton3.Name = "bunifuFlatButton3";
            this.bunifuFlatButton3.Normalcolor = System.Drawing.Color.DarkCyan;
            this.bunifuFlatButton3.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.bunifuFlatButton3.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton3.selected = false;
            this.bunifuFlatButton3.Size = new System.Drawing.Size(204, 58);
            this.bunifuFlatButton3.TabIndex = 10;
            this.bunifuFlatButton3.Text = "                Delete";
            this.bunifuFlatButton3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton3.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton3.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton3.Click += new System.EventHandler(this.bunifuFlatButton3_Click);
            // 
            // currentimage10
            // 
            this.currentimage10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage10.Location = new System.Drawing.Point(1397, 414);
            this.currentimage10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage10.Name = "currentimage10";
            this.currentimage10.Size = new System.Drawing.Size(140, 129);
            this.currentimage10.TabIndex = 113;
            this.currentimage10.Click += new System.EventHandler(this.currentimage10_Click);
            // 
            // currentimage9
            // 
            this.currentimage9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage9.Location = new System.Drawing.Point(1245, 414);
            this.currentimage9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage9.Name = "currentimage9";
            this.currentimage9.Size = new System.Drawing.Size(140, 129);
            this.currentimage9.TabIndex = 112;
            this.currentimage9.Click += new System.EventHandler(this.currentimage9_Click);
            // 
            // currentimage8
            // 
            this.currentimage8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage8.Location = new System.Drawing.Point(1093, 414);
            this.currentimage8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage8.Name = "currentimage8";
            this.currentimage8.Size = new System.Drawing.Size(140, 129);
            this.currentimage8.TabIndex = 111;
            this.currentimage8.Click += new System.EventHandler(this.currentimage8_Click);
            // 
            // currentimage7
            // 
            this.currentimage7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage7.Location = new System.Drawing.Point(941, 414);
            this.currentimage7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage7.Name = "currentimage7";
            this.currentimage7.Size = new System.Drawing.Size(140, 129);
            this.currentimage7.TabIndex = 110;
            this.currentimage7.Click += new System.EventHandler(this.currentimage7_Click);
            // 
            // currentimage6
            // 
            this.currentimage6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage6.Location = new System.Drawing.Point(789, 414);
            this.currentimage6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage6.Name = "currentimage6";
            this.currentimage6.Size = new System.Drawing.Size(140, 129);
            this.currentimage6.TabIndex = 109;
            this.currentimage6.Click += new System.EventHandler(this.currentimage6_Click);
            // 
            // currentimage5
            // 
            this.currentimage5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage5.Location = new System.Drawing.Point(638, 414);
            this.currentimage5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage5.Name = "currentimage5";
            this.currentimage5.Size = new System.Drawing.Size(140, 129);
            this.currentimage5.TabIndex = 108;
            this.currentimage5.Click += new System.EventHandler(this.currentimage5_Click);
            // 
            // currentimage4
            // 
            this.currentimage4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage4.Location = new System.Drawing.Point(488, 414);
            this.currentimage4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage4.Name = "currentimage4";
            this.currentimage4.Size = new System.Drawing.Size(140, 129);
            this.currentimage4.TabIndex = 107;
            this.currentimage4.Click += new System.EventHandler(this.currentimage4_Click);
            // 
            // currentimage3
            // 
            this.currentimage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage3.Location = new System.Drawing.Point(340, 414);
            this.currentimage3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage3.Name = "currentimage3";
            this.currentimage3.Size = new System.Drawing.Size(140, 129);
            this.currentimage3.TabIndex = 106;
            this.currentimage3.Click += new System.EventHandler(this.currentimage3_Click);
            // 
            // currentimage2
            // 
            this.currentimage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage2.Location = new System.Drawing.Point(192, 414);
            this.currentimage2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage2.Name = "currentimage2";
            this.currentimage2.Size = new System.Drawing.Size(140, 129);
            this.currentimage2.TabIndex = 105;
            this.currentimage2.Click += new System.EventHandler(this.currentimage2_Click);
            // 
            // currentimage1
            // 
            this.currentimage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage1.Location = new System.Drawing.Point(42, 414);
            this.currentimage1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage1.Name = "currentimage1";
            this.currentimage1.Size = new System.Drawing.Size(140, 129);
            this.currentimage1.TabIndex = 104;
            this.currentimage1.Click += new System.EventHandler(this.currentimage1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(46, 419);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(127, 117);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 91;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Location = new System.Drawing.Point(197, 419);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(127, 117);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 92;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            this.pictureBox2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox2.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox3.Location = new System.Drawing.Point(345, 420);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(127, 117);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 93;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            this.pictureBox3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox3.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox4.Location = new System.Drawing.Point(493, 420);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(127, 117);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 94;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            this.pictureBox4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox4.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox5.Location = new System.Drawing.Point(644, 420);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(127, 117);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 95;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBox5_Click);
            this.pictureBox5.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox5.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox6.Location = new System.Drawing.Point(794, 420);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(127, 117);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 96;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Click += new System.EventHandler(this.pictureBox6_Click);
            this.pictureBox6.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox6.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox7.Location = new System.Drawing.Point(946, 420);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(127, 117);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 97;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Click += new System.EventHandler(this.pictureBox7_Click_1);
            this.pictureBox7.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox7.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox8.Location = new System.Drawing.Point(1097, 420);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(127, 117);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 98;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Click += new System.EventHandler(this.pictureBox8_Click);
            this.pictureBox8.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox8.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox9.Location = new System.Drawing.Point(1249, 420);
            this.pictureBox9.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(127, 117);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 99;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.Click += new System.EventHandler(this.pictureBox9_Click);
            this.pictureBox9.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox9.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox10.Location = new System.Drawing.Point(1402, 420);
            this.pictureBox10.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(127, 117);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 103;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.Click += new System.EventHandler(this.pictureBox10_Click);
            this.pictureBox10.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox10.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // currentimage11
            // 
            this.currentimage11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage11.Location = new System.Drawing.Point(41, 560);
            this.currentimage11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage11.Name = "currentimage11";
            this.currentimage11.Size = new System.Drawing.Size(140, 129);
            this.currentimage11.TabIndex = 115;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox11.Location = new System.Drawing.Point(46, 566);
            this.pictureBox11.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(127, 117);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 114;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox11.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // currentimage20
            // 
            this.currentimage20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage20.Location = new System.Drawing.Point(1397, 560);
            this.currentimage20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage20.Name = "currentimage20";
            this.currentimage20.Size = new System.Drawing.Size(140, 129);
            this.currentimage20.TabIndex = 133;
            // 
            // currentimage19
            // 
            this.currentimage19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage19.Location = new System.Drawing.Point(1245, 560);
            this.currentimage19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage19.Name = "currentimage19";
            this.currentimage19.Size = new System.Drawing.Size(140, 129);
            this.currentimage19.TabIndex = 132;
            // 
            // currentimage18
            // 
            this.currentimage18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage18.Location = new System.Drawing.Point(1093, 560);
            this.currentimage18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage18.Name = "currentimage18";
            this.currentimage18.Size = new System.Drawing.Size(140, 129);
            this.currentimage18.TabIndex = 131;
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox23.Location = new System.Drawing.Point(1097, 566);
            this.pictureBox23.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(127, 117);
            this.pictureBox23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox23.TabIndex = 128;
            this.pictureBox23.TabStop = false;
            this.pictureBox23.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox23.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox22.Location = new System.Drawing.Point(1249, 566);
            this.pictureBox22.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(127, 117);
            this.pictureBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox22.TabIndex = 129;
            this.pictureBox22.TabStop = false;
            this.pictureBox22.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox22.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox21.Location = new System.Drawing.Point(1402, 566);
            this.pictureBox21.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(127, 117);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox21.TabIndex = 130;
            this.pictureBox21.TabStop = false;
            this.pictureBox21.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox21.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // currentimage30
            // 
            this.currentimage30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage30.Location = new System.Drawing.Point(1396, 707);
            this.currentimage30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage30.Name = "currentimage30";
            this.currentimage30.Size = new System.Drawing.Size(140, 129);
            this.currentimage30.TabIndex = 153;
            // 
            // currentimage29
            // 
            this.currentimage29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage29.Location = new System.Drawing.Point(1244, 707);
            this.currentimage29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage29.Name = "currentimage29";
            this.currentimage29.Size = new System.Drawing.Size(140, 129);
            this.currentimage29.TabIndex = 152;
            // 
            // currentimage28
            // 
            this.currentimage28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage28.Location = new System.Drawing.Point(1093, 707);
            this.currentimage28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage28.Name = "currentimage28";
            this.currentimage28.Size = new System.Drawing.Size(140, 129);
            this.currentimage28.TabIndex = 151;
            // 
            // pictureBox31
            // 
            this.pictureBox31.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox31.Location = new System.Drawing.Point(1098, 713);
            this.pictureBox31.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(127, 117);
            this.pictureBox31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox31.TabIndex = 148;
            this.pictureBox31.TabStop = false;
            this.pictureBox31.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox31.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox30
            // 
            this.pictureBox30.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox30.Location = new System.Drawing.Point(1249, 713);
            this.pictureBox30.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(127, 117);
            this.pictureBox30.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox30.TabIndex = 149;
            this.pictureBox30.TabStop = false;
            this.pictureBox30.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox30.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox29
            // 
            this.pictureBox29.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox29.Location = new System.Drawing.Point(1402, 713);
            this.pictureBox29.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(127, 117);
            this.pictureBox29.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox29.TabIndex = 150;
            this.pictureBox29.TabStop = false;
            this.pictureBox29.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox29.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // currentimage21
            // 
            this.currentimage21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage21.Location = new System.Drawing.Point(40, 707);
            this.currentimage21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage21.Name = "currentimage21";
            this.currentimage21.Size = new System.Drawing.Size(140, 129);
            this.currentimage21.TabIndex = 135;
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox28.Location = new System.Drawing.Point(45, 713);
            this.pictureBox28.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(127, 117);
            this.pictureBox28.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox28.TabIndex = 134;
            this.pictureBox28.TabStop = false;
            this.pictureBox28.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox28.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // currentimage12
            // 
            this.currentimage12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage12.Location = new System.Drawing.Point(190, 560);
            this.currentimage12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage12.Name = "currentimage12";
            this.currentimage12.Size = new System.Drawing.Size(140, 129);
            this.currentimage12.TabIndex = 117;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox15.Location = new System.Drawing.Point(195, 566);
            this.pictureBox15.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(127, 117);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox15.TabIndex = 116;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox15.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // currentimage17
            // 
            this.currentimage17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage17.Location = new System.Drawing.Point(941, 560);
            this.currentimage17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage17.Name = "currentimage17";
            this.currentimage17.Size = new System.Drawing.Size(140, 129);
            this.currentimage17.TabIndex = 127;
            // 
            // currentimage16
            // 
            this.currentimage16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage16.Location = new System.Drawing.Point(789, 560);
            this.currentimage16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage16.Name = "currentimage16";
            this.currentimage16.Size = new System.Drawing.Size(140, 129);
            this.currentimage16.TabIndex = 126;
            // 
            // currentimage15
            // 
            this.currentimage15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage15.Location = new System.Drawing.Point(638, 560);
            this.currentimage15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage15.Name = "currentimage15";
            this.currentimage15.Size = new System.Drawing.Size(140, 129);
            this.currentimage15.TabIndex = 125;
            // 
            // currentimage14
            // 
            this.currentimage14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage14.Location = new System.Drawing.Point(488, 560);
            this.currentimage14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage14.Name = "currentimage14";
            this.currentimage14.Size = new System.Drawing.Size(140, 129);
            this.currentimage14.TabIndex = 124;
            // 
            // currentimage13
            // 
            this.currentimage13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage13.Location = new System.Drawing.Point(340, 560);
            this.currentimage13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage13.Name = "currentimage13";
            this.currentimage13.Size = new System.Drawing.Size(140, 129);
            this.currentimage13.TabIndex = 123;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox20.Location = new System.Drawing.Point(345, 566);
            this.pictureBox20.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(127, 117);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox20.TabIndex = 118;
            this.pictureBox20.TabStop = false;
            this.pictureBox20.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox20.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox19.Location = new System.Drawing.Point(493, 566);
            this.pictureBox19.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(127, 117);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox19.TabIndex = 119;
            this.pictureBox19.TabStop = false;
            this.pictureBox19.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox19.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox18.Location = new System.Drawing.Point(644, 566);
            this.pictureBox18.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(127, 117);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox18.TabIndex = 120;
            this.pictureBox18.TabStop = false;
            this.pictureBox18.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox18.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox17.Location = new System.Drawing.Point(794, 566);
            this.pictureBox17.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(127, 117);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox17.TabIndex = 121;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox17.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox16.Location = new System.Drawing.Point(946, 566);
            this.pictureBox16.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(127, 117);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox16.TabIndex = 122;
            this.pictureBox16.TabStop = false;
            this.pictureBox16.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox16.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // currentimage22
            // 
            this.currentimage22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage22.Location = new System.Drawing.Point(189, 707);
            this.currentimage22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage22.Name = "currentimage22";
            this.currentimage22.Size = new System.Drawing.Size(140, 129);
            this.currentimage22.TabIndex = 137;
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox27.Location = new System.Drawing.Point(194, 713);
            this.pictureBox27.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(127, 117);
            this.pictureBox27.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox27.TabIndex = 136;
            this.pictureBox27.TabStop = false;
            this.pictureBox27.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox27.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // currentimage25
            // 
            this.currentimage25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage25.Location = new System.Drawing.Point(637, 707);
            this.currentimage25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage25.Name = "currentimage25";
            this.currentimage25.Size = new System.Drawing.Size(140, 129);
            this.currentimage25.TabIndex = 143;
            // 
            // currentimage24
            // 
            this.currentimage24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage24.Location = new System.Drawing.Point(487, 707);
            this.currentimage24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage24.Name = "currentimage24";
            this.currentimage24.Size = new System.Drawing.Size(140, 129);
            this.currentimage24.TabIndex = 142;
            // 
            // currentimage23
            // 
            this.currentimage23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage23.Location = new System.Drawing.Point(339, 707);
            this.currentimage23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage23.Name = "currentimage23";
            this.currentimage23.Size = new System.Drawing.Size(140, 129);
            this.currentimage23.TabIndex = 141;
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox26.Location = new System.Drawing.Point(344, 713);
            this.pictureBox26.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(127, 117);
            this.pictureBox26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox26.TabIndex = 138;
            this.pictureBox26.TabStop = false;
            this.pictureBox26.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox26.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox25.Location = new System.Drawing.Point(492, 713);
            this.pictureBox25.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(127, 117);
            this.pictureBox25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox25.TabIndex = 139;
            this.pictureBox25.TabStop = false;
            this.pictureBox25.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox25.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox24.Location = new System.Drawing.Point(643, 713);
            this.pictureBox24.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(127, 117);
            this.pictureBox24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox24.TabIndex = 140;
            this.pictureBox24.TabStop = false;
            this.pictureBox24.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox24.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // currentimage26
            // 
            this.currentimage26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage26.Location = new System.Drawing.Point(788, 707);
            this.currentimage26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage26.Name = "currentimage26";
            this.currentimage26.Size = new System.Drawing.Size(140, 129);
            this.currentimage26.TabIndex = 145;
            // 
            // pictureBox33
            // 
            this.pictureBox33.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox33.Location = new System.Drawing.Point(793, 713);
            this.pictureBox33.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox33.Name = "pictureBox33";
            this.pictureBox33.Size = new System.Drawing.Size(127, 117);
            this.pictureBox33.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox33.TabIndex = 144;
            this.pictureBox33.TabStop = false;
            this.pictureBox33.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox33.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // currentimage27
            // 
            this.currentimage27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.currentimage27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.currentimage27.Location = new System.Drawing.Point(941, 707);
            this.currentimage27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentimage27.Name = "currentimage27";
            this.currentimage27.Size = new System.Drawing.Size(140, 129);
            this.currentimage27.TabIndex = 147;
            // 
            // pictureBox32
            // 
            this.pictureBox32.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pictureBox32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox32.Location = new System.Drawing.Point(946, 713);
            this.pictureBox32.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(127, 117);
            this.pictureBox32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox32.TabIndex = 146;
            this.pictureBox32.TabStop = false;
            this.pictureBox32.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox32.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxMouseDoubleClick);
            // 
            // S_Gems
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.ClientSize = new System.Drawing.Size(1575, 935);
            this.Controls.Add(this.pictureBox29);
            this.Controls.Add(this.pictureBox30);
            this.Controls.Add(this.pictureBox31);
            this.Controls.Add(this.currentimage28);
            this.Controls.Add(this.currentimage29);
            this.Controls.Add(this.currentimage30);
            this.Controls.Add(this.pictureBox32);
            this.Controls.Add(this.currentimage27);
            this.Controls.Add(this.pictureBox33);
            this.Controls.Add(this.currentimage26);
            this.Controls.Add(this.pictureBox24);
            this.Controls.Add(this.pictureBox25);
            this.Controls.Add(this.pictureBox26);
            this.Controls.Add(this.currentimage23);
            this.Controls.Add(this.currentimage24);
            this.Controls.Add(this.currentimage25);
            this.Controls.Add(this.pictureBox27);
            this.Controls.Add(this.currentimage22);
            this.Controls.Add(this.pictureBox28);
            this.Controls.Add(this.currentimage21);
            this.Controls.Add(this.pictureBox21);
            this.Controls.Add(this.pictureBox22);
            this.Controls.Add(this.pictureBox23);
            this.Controls.Add(this.currentimage18);
            this.Controls.Add(this.currentimage19);
            this.Controls.Add(this.currentimage20);
            this.Controls.Add(this.pictureBox16);
            this.Controls.Add(this.pictureBox17);
            this.Controls.Add(this.pictureBox18);
            this.Controls.Add(this.pictureBox19);
            this.Controls.Add(this.pictureBox20);
            this.Controls.Add(this.currentimage13);
            this.Controls.Add(this.currentimage14);
            this.Controls.Add(this.currentimage15);
            this.Controls.Add(this.currentimage16);
            this.Controls.Add(this.currentimage17);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.currentimage12);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.currentimage11);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.btnrefresh);
            this.Controls.Add(this.btnupdate);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.TB_File_Path);
            this.Controls.Add(this.txt_gems);
            this.Controls.Add(this.txt_weight);
            this.Controls.Add(this.txt_cost);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.hello);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cmbStockType);
            this.Controls.Add(this.bunifuFlatButton3);
            this.Controls.Add(this.Stock_Type);
            this.Controls.Add(this.txt_ID);
            this.Controls.Add(this.currentimage1);
            this.Controls.Add(this.currentimage2);
            this.Controls.Add(this.currentimage3);
            this.Controls.Add(this.currentimage4);
            this.Controls.Add(this.currentimage5);
            this.Controls.Add(this.currentimage6);
            this.Controls.Add(this.currentimage7);
            this.Controls.Add(this.currentimage8);
            this.Controls.Add(this.currentimage9);
            this.Controls.Add(this.currentimage10);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "S_Gems";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "S_Gems";
            this.Load += new System.EventHandler(this.S_Gems_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox Stock_Type;
        public System.Windows.Forms.TextBox txt_ID;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label hello;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        public System.Windows.Forms.TextBox TB_File_Path;
        public Bunifu.Framework.UI.BunifuFlatButton btnupdate;
        public Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton3;
        public Bunifu.Framework.UI.BunifuFlatButton btnrefresh;
        public Bunifu.Framework.UI.BunifuFlatButton btnsave;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.ComboBox txt_gems;
        public System.Windows.Forms.TextBox txt_weight;
        public System.Windows.Forms.TextBox txt_cost;
        public System.Windows.Forms.ComboBox cmbStockType;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.TextBox txtstock_no;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtno_of_peices;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.PictureBox pictureBox10;
        public System.Windows.Forms.PictureBox pictureBox9;
        public System.Windows.Forms.PictureBox pictureBox8;
        public System.Windows.Forms.PictureBox pictureBox7;
        public System.Windows.Forms.PictureBox pictureBox6;
        public System.Windows.Forms.PictureBox pictureBox5;
        public System.Windows.Forms.PictureBox pictureBox4;
        public System.Windows.Forms.PictureBox pictureBox3;
        public System.Windows.Forms.PictureBox pictureBox2;
        public System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label currentimage1;
        private System.Windows.Forms.Label currentimage2;
        private System.Windows.Forms.Label currentimage3;
        private System.Windows.Forms.Label currentimage4;
        private System.Windows.Forms.Label currentimage5;
        private System.Windows.Forms.Label currentimage6;
        private System.Windows.Forms.Label currentimage7;
        private System.Windows.Forms.Label currentimage8;
        private System.Windows.Forms.Label currentimage9;
        private System.Windows.Forms.Label currentimage10;
        public System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Label currentimage11;
        public System.Windows.Forms.PictureBox pictureBox29;
        public System.Windows.Forms.PictureBox pictureBox30;
        public System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.Label currentimage28;
        private System.Windows.Forms.Label currentimage29;
        private System.Windows.Forms.Label currentimage30;
        public System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.Label currentimage27;
        public System.Windows.Forms.PictureBox pictureBox33;
        private System.Windows.Forms.Label currentimage26;
        public System.Windows.Forms.PictureBox pictureBox24;
        public System.Windows.Forms.PictureBox pictureBox25;
        public System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.Label currentimage23;
        private System.Windows.Forms.Label currentimage24;
        private System.Windows.Forms.Label currentimage25;
        public System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.Label currentimage22;
        public System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.Label currentimage21;
        public System.Windows.Forms.PictureBox pictureBox21;
        public System.Windows.Forms.PictureBox pictureBox22;
        public System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.Label currentimage18;
        private System.Windows.Forms.Label currentimage19;
        private System.Windows.Forms.Label currentimage20;
        public System.Windows.Forms.PictureBox pictureBox16;
        public System.Windows.Forms.PictureBox pictureBox17;
        public System.Windows.Forms.PictureBox pictureBox18;
        public System.Windows.Forms.PictureBox pictureBox19;
        public System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.Label currentimage13;
        private System.Windows.Forms.Label currentimage14;
        private System.Windows.Forms.Label currentimage15;
        private System.Windows.Forms.Label currentimage16;
        private System.Windows.Forms.Label currentimage17;
        public System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.Label currentimage12;
    }
}